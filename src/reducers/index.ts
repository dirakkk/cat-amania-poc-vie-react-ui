import { CommonAction, InitData } from '../actions'
import Dossier from 'cat-amania-poc-vie-dossier/dist';

export interface PocState {
    initData?: InitData
    step: number
    loading: boolean
    currentDossierId?: string
}

const initialState: PocState = {
    step: 0,
    loading: false
};

export const rootReducer = (state: PocState = initialState, action: CommonAction<any>): PocState => {
    switch (action.type) {
        case 'LOGIN':
            return ({ ...state, loading: true })
        case 'LOGIN_SUCCESS':
            const initData = action.payload //No type safe
            return ({ ...state, step: 1, loading: false, initData })
        case 'OUVRIR_DOSSIER':
            return ({ ...state, loading: true })
        case 'OUVRIR_DOSSIER_SUCCESS':
            return ({ ...state, step: 2, loading: false })
        default:
            return (state)
    }
}

export const dossierReducer = (state: Dossier, action: CommonAction<any>): Dossier => {
    switch (action.type) {
        case 'OUVRIR_DOSSIER_SUCCESS':
            const dossier = action.payload //No type safe
            return ({ ...state, ...dossier })
        default:
            return (state)
    }
}
