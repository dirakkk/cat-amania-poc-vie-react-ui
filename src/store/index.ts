import { createStore, combineReducers, applyMiddleware } from 'redux'
import { rootReducer , dossierReducer } from '../reducers/index'
import Dossier from 'cat-amania-poc-vie-dossier'
import { createForms } from 'react-redux-form';
import { PocState } from '../reducers'
import middleWare from '../epics/dossierEpics';

export interface AppState {
    root: PocState
    dossier: Dossier
}

const store: any = createStore(
    combineReducers({
        root: rootReducer,
        // ... use createForms, which will create:
        // the model reducer at "user"
        // the forms reducer at "forms" (e.g., "forms.user")
        ...createForms({
            dossier: dossierReducer,
        }),
    }),
    applyMiddleware(middleWare)
);

export default store