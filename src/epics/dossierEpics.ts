import { MiddlewareAPI, Action } from 'redux'
import { actions } from 'react-redux-form'
import 'rxjs/add/operator/last';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/delay';
import { AppState } from '../store/index';
import { CommonAction, loginSuccessAction, buildNoPayloadAction, openDossierSuccessAction } from '../actions/index';
import { createEpicMiddleware, Epic, ActionsObservable, combineEpics } from 'redux-observable';
import { login, getDossier } from '../common/serverApi'

const loginRequestEpic: Epic<CommonAction<any>, AppState> = (
  action$: ActionsObservable<CommonAction<any>>,
  store: MiddlewareAPI<AppState>,
) => {
  return action$
    .ofType('LOGIN')
    .delay(3000)
    .mergeMap(
    action => {
      return login({ email: 'moi', password: 'pass' })
        .then(response => {
          console.log('response:', response)
          actions.change('dossier', response)
          return loginSuccessAction(response)
        }
        )
        .catch(error => buildNoPayloadAction('LOGIN_FAILURE'))
    }
    )
}

const ouvrirDossierExistantEpic: Epic<CommonAction<any>, AppState> = (
  action$: ActionsObservable<CommonAction<any>>,
  store: MiddlewareAPI<AppState>,
) => {
  return action$
    .ofType('OUVRIR_DOSSIER')
    .delay(3000)
    .mergeMap(
    action => {
      return getDossier(action.payload)
        .then(response => {
          console.log('response:', response)
          return openDossierSuccessAction(response)
        }
        )
        .catch(error => buildNoPayloadAction('OPEN_DOSSIER_FAILURE'))
    }
    )
}

const autoSavDossierEpic: Epic<Action, AppState> = (
  action$: ActionsObservable<Action>,
  store: MiddlewareAPI<AppState>,
) => {
  return action$
    .ofType('rrf/change')
    .delay(3000)
    .mergeMap(
    action => {
      console.log('in epic autosav:', action)
      return getDossier(action.type)
        .then(response => {
          console.log('response:', response)
          return buildNoPayloadAction('SAVED_MODIFIED_FIELD')
        }
        )
        .catch(error => buildNoPayloadAction('OPEN_DOSSIER_FAILURE'))

    }
    )
}

const combinedEpics = combineEpics(loginRequestEpic, ouvrirDossierExistantEpic, autoSavDossierEpic)
const middleWare = createEpicMiddleware(combinedEpics)
export default middleWare