import * as React from 'react'
import '../App.css'
import { Form, Control } from 'react-redux-form'
import Dossier from 'cat-amania-poc-vie-dossier'
import { Grid, Row, Col, Button, Panel } from 'react-bootstrap';
import { Header } from 'react-bootstrap/lib/Modal';
import { Footer } from 'react-bootstrap/lib/Panel';
import { AppState } from '../store/index';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { loginAction, CommonAction } from '../actions/index';
import { RouteComponentProps } from 'react-router';
import { PulseLoader } from 'react-spinners'

interface Props extends RouteComponentProps<any> {
    loading: boolean
    step: number
    dossier: Dossier
    dispatchLoginAction: (userName: string) => CommonAction<any>
}

interface State {
    value: string
}

class LoginForm extends React.Component<Props, State> {
    public constructor(props: Props) {
        super(props)
        this.state = {
            value: new Date().toISOString(),
        }
    }

    componentWillReceiveProps(nextProps: Props) {
        if (nextProps.step === 1) {
            this.props.history.push('/choixsession')
        }
    }

    render() {
        return (
            <Form
                model={'dossier'}
                hideNativeErrors={false}
            >
                <Grid>
                    <Header step={0} />
                    {this.buildChapitreLogin()}
                    <Footer />
                </Grid>
            </Form>
        );
    }

    public validerFormulaire = () => {
        this.props.dispatchLoginAction('wazzza')
    }

    public boutonPoursuivre = () => (
        <Button bsStyle="primary" onClick={this.validerFormulaire}>
            {this.props.loading ? <PulseLoader color="#36D7B7" /> : 'Poursuivre'}
        </Button>
    )

    public buildChapitreLogin(): any {
        return (
            <Row>
                <Col xs={3} />
                <Col xs={6}>
                    <Panel bsStyle="primary" id="collapsible-panel-example-1" defaultExpanded={true} >
                        <Panel.Heading>
                            <Panel.Title componentClass="h3" toggle={true} >Authentication</Panel.Title>
                        </Panel.Heading>
                        <Panel.Collapse>
                            <Panel.Body style={{ backgroundColor: '#CCCC' }}>
                                <Row style={{ backgroundColor: '#CCCC' }}>
                                    <Row>
                                        <Col xsOffset={2} xs={4} >Nom conseiller</Col>
                                        <Col xsOffset={1} xs={4} >
                                            {this.buildControl('conseiller', 'nom')}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xsOffset={2} xs={4}>Password</Col>
                                        <Col xsOffset={1} xs={4}>
                                            <Control.text model="conseiller.password" />
                                        </Col>
                                    </Row>
                                    <Row />
                                    <Row>
                                        <Col xsOffset={4} xs={6} sm={3} md={2}>{this.boutonPoursuivre()}</Col>
                                    </Row>
                                </Row>
                            </Panel.Body>
                        </Panel.Collapse>
                    </Panel>
                </Col>
                <Col xs={3} />
            </Row>
        );
    }

    public buildControl<K extends keyof Dossier, L extends keyof Dossier[K]>(key1: K, key2: L) {
        const path = `.${key1}.${key2}`
        return (
            <Control.text model={path} />
        )
    }
}

const mapStateToProps = (state: AppState) => ({
    loading: state.root.loading,
    step: state.root.step,
    dossier: state.dossier,
});

const mapDispatchToProps = (dispatch: Dispatch<Props>) => ({
    dispatchLoginAction: (userName: string) => dispatch(loginAction),
})

//export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LoginForm))
export default connect(mapStateToProps, mapDispatchToProps)(LoginForm)