import * as React from 'react'
import '../App.css'
import { Form, Control } from 'react-redux-form'
import { Grid, Row, Col, Button, Panel, Radio } from 'react-bootstrap';
import { Header } from 'react-bootstrap/lib/Modal';
import { Footer } from 'react-bootstrap/lib/Panel';
import { AppState } from '../store/index';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { CommonAction, InitData, buildNoPayloadAction, ouvrirDossierExistantAction } from '../actions/index';
import { RouteComponentProps } from 'react-router';
import { PulseLoader } from 'react-spinners'

interface Props extends RouteComponentProps<any> {
    loading: boolean
    step: number
    initData: InitData
    selectedDossier: number
    dispatchOuvrirDossierAction: (dossierId: string) => CommonAction<any>
    dispatchCreerDossierAction: () => CommonAction<any>
}

interface State {
    choix: string
}

class ChoixSessionForm extends React.Component<Props, State> {
    public constructor(props: Props) {
        super(props)
        this.state = {
            choix: 'existant',
        }
    }

    componentWillReceiveProps(nextProps: Props) {
        const { step, selectedDossier} = nextProps
        if (step === 2) {
            this.props.history.push(`/verification/:${selectedDossier}`)
        }
    }

    render() {
        return (
            <Form
                model={'dossier'}
                hideNativeErrors={false}
            >
                <Grid>
                    <Header step={0} />
                    {this.buildChapitreChoixSession()}
                    <Footer />
                </Grid>
            </Form>
        );
    }

    public validerFormulaire = () => {
        if (this.state.choix === 'existant') {
            const { selectedDossier } = this.props
            console.log(' selectedDossier ', selectedDossier)
            this.props.dispatchOuvrirDossierAction(String(selectedDossier))
        } else {
            this.props.dispatchCreerDossierAction()
        }
    }

    public boutonPoursuivre = () => (
        <Button bsStyle="primary" onClick={this.validerFormulaire}>
            {this.props.loading ? <PulseLoader color="#36D7B7" /> : 'Poursuivre'}
        </Button>
    )

    public buildChapitreChoixSession(): any {
        return (
            <Row>
                <Col xs={3} />
                <Col xs={6}>
                    <Panel bsStyle="primary" id="collapsible-panel-example-1" defaultExpanded={true} >
                        <Panel.Heading>
                            <Panel.Title componentClass="h3" toggle={true} >Authentication</Panel.Title>
                        </Panel.Heading>
                        <Panel.Collapse>
                            <Panel.Body style={{ backgroundColor: '#CCCC' }}>
                                {this.buildRadioChoice()}
                            </Panel.Body>
                        </Panel.Collapse>
                    </Panel>
                </Col>
                <Col xs={3} />
            </Row>
        );
    }

    public creerNouveauDossier = () => (
        <Row>
            <Col xsOffset={2} xs={8}>Creer un nouveau dossier</Col>
        </Row>
    )

    public buildChoixDossierExistant = () => (
        <Row>
            <Col xsOffset={2} xs={4} >Ouvrir un dossier existant</Col>
            <Col xsOffset={1} xs={4} >
                <Control.select disabled={this.state.choix !== 'existant'} model={'.id'}>
                    {this.props.initData.idDossiers.map((id: string, it: number) =>
                        <option key={it} value={id}>{id}</option>
                    )
                    }
                </Control.select>
            </Col>
        </Row>
    )

    public setRadioChoix = (choix: string) => {
        console.log('in setradiochoix')
        this.setState({ choix })
    }

    public buildRadioChoice = () => (
        <Row style={{ backgroundColor: '#CCCC' }}>
            <Radio
                name="radioGroup"
                checked={this.state.choix === 'existant'}
                onChange={(e) => this.setRadioChoix('existant')}
            >
                {this.buildChoixDossierExistant()}
            </Radio>
            <Radio name="radioGroup" onChange={(e) => this.setRadioChoix('nouveau')}>
                {this.creerNouveauDossier()}
            </Radio>
            <Row>
                <Col xsOffset={4} xs={6} sm={3} md={2}>{this.boutonPoursuivre()}</Col>
            </Row>
        </Row>
    )
}

const mapStateToProps = (state: AppState) => ({
    loading: state.root.loading,
    step: state.root.step,
    initData: state.root.initData,
    selectedDossier: state.dossier.id
});

const mapDispatchToProps = (dispatch: Dispatch<Props>) => ({
    dispatchOuvrirDossierAction: (dossierId: string) => dispatch(ouvrirDossierExistantAction(dossierId)),
    dispatchCreerDossierAction: () => dispatch(buildNoPayloadAction('CREATE_NEW_DOSSIER'))
})

export default connect(mapStateToProps, mapDispatchToProps)(ChoixSessionForm)