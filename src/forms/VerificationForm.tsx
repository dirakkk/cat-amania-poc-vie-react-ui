import * as React from 'react'
import '../App.css'
import Chapitre from '../components/Chapitre'
import Header from '../components/Header'
import { Form, Control, actions, ModelAction } from 'react-redux-form'
import { Grid, Row, Col } from 'react-bootstrap'
import Footer from '../components/Footer'
import { connect, Dispatch } from 'react-redux'
import Dossier from 'cat-amania-poc-vie-dossier'
import { AppState } from '../store'
import { format, parse } from 'date-fns'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import * as moment from 'moment'
import '../App.css'
import { RouteComponentProps } from 'react-router';
import Autosuggest from 'react-bootstrap-autosuggest'
import { ouvrirDossierExistantAction, CommonAction } from '../actions/index';

interface Props extends RouteComponentProps<any> {
  dossier: Dossier
  step: number
  dispatchChangeAction: (model: string, value: any) => ModelAction
  dispatchOuvrirDossierAction: (dossierId: string) => CommonAction<any>
}

interface State {
  value: string
}

class VerificationForm extends React.Component<Props, State> {

  public constructor(props: Props) {
    super(props)
    this.state = {
      value: new Date().toISOString(),
    }
  }

  public isEmail = (val?: string) =>
    val ? val.includes('@') : false;
  public isRequired = (val?: string) =>
    val ? val.length > 0 : false;

  // tslint:disable-next-line:member-ordering
  public emailValidator = {
    isEmail: this.isEmail,
    isRequired: this.isRequired
  };

  public handleChange(form: any): (modelValue: any) => void {
    return (modelValue: any) => {
      console.log('modelValue');
    };
  }

  public handleCalendarChange = (value: any): any => {
    this.setState({
      value: moment(value).toISOString(),
    })
    this.props.dispatchChangeAction('dossier.informations.adherent.dateDeNaissance', value)
  }

  public handleSubmit(values: any): void {
    console.log('submit');
  }

  componentWillReceiveProps(nextProps: Props) {
    if (nextProps.step === 3) {
      this.props.history.push('/personne')
    } else if (nextProps.dossier.conseiller === undefined) {
      console.log('step:', nextProps.step, 'we need to reload')
    }
  }

  /*
  shouldComponentUpdate(nextProps: Props, nextState: State) {
    const { step, dispatchOuvrirDossierAction } = nextProps
    if (step === 0) {
      console.log('step:', step, 'we need to reload')
      dispatchOuvrirDossierAction('12')
      return false
    } else {
      return true
    }
  }
  */

  render() {
    const { step, dispatchOuvrirDossierAction } = this.props
    if (step === 0) {
      console.log('step:', step, 'we need to reload')
      dispatchOuvrirDossierAction('12')
      return (<div> waiting</div>)
    } else {
      return this.renderForm('dossier')
    }
  }

  renderForm(key: keyof AppState) {
    const { dossier: { informations } } = this.props
    return (
      <Form
        model={key}
        hideNativeErrors={false}
        onChange={this.handleChange}
      >
        <Grid>
          <Header step={0} />
          {this.buildChapitreConseiller()}
          <Chapitre title="Dossier">
            <Row>Adhérent</Row>
            {this.buildSectionAdherent()}
            <Row style={{ marginTop: '20px' }} >Particularité</Row>
            {this.buildSectionParticularite()}
            <Row>Coordonnées de contact</Row>
            {this.buildSectionCoordContact('informations', 'coordonneesContact')}
          </Chapitre>
          {this.buildChapitreQCE()}
          {informations.particularites.estSousRegimeDeProtection ? this.buildAdhesion() : null}
          <Footer />
        </Grid>
      </Form>
    );
  }

  public buildChapitreConseiller(): any {
    return (
      <Chapitre title="Conseiller ayant initié dossier">
        <Row style={{ backgroundColor: '#CCCC' }}>
          <Col xs={6} sm={3} md={2}>Code point de vente</Col>
          <Col xs={6} sm={3} md={2}>{this.buildCodePointdeVenteSeeker()}</Col>
          <Col xs={6} sm={3} md={2}>Numéro de conseiller</Col>
          <Col xs={6} sm={3} md={2}>
            <Control.text model=".conseiller.numeroDeConseiller" />
          </Col>
          <Col xs={6} sm={3} md={1}>Nom prenom conseiller</Col>
          <Col xs={6} sm={3} md={2}>
            <Control.text model=".conseiller.nom" />
          </Col>
        </Row>
      </Chapitre>
    );
  }

  public changeAndSubmit = (model: string, value: string) => {
    console.log('value de date', value, model)
    this.props.dispatchChangeAction(model, parse(value).toISOString())
    //dispatch(actions.submit('user', submitPromise));
  }

  public buildSectionAdherent(): any {
    const { dossier: { informations: { adherent: { dateDeNaissance } } } } = this.props
    return (
      <Row style={{ backgroundColor: '#CCCC', height: '50' }}>
        <Col xs={6} sm={3} md={2}>Nom de naissance</Col>
        <Col xs={6} sm={3} md={2}>{<Control.text model=".informations.adherent.nom" />}</Col>
        <Col xs={6} sm={3} md={2}>Date de naissance</Col>
        <Col xs={6} sm={3} md={2}>
          <DatePicker
            value={format(dateDeNaissance, 'DD/MM/YYYY')}
            onChange={this.handleCalendarChange}
            dateFormat="LLL"
          />
          {/*<Control.text
            model=".informations.adherent.dateDeNaissance"
            mapProps={{ value: ({ modelValue }) => format(modelValue, 'DD/MM/YYYY') }}
            changeAction={this.changeAndSubmit}
          />
          */}
        </Col>
      </Row>
    );
  }

  public buildSectionParticularite = () => (
    <Row style={{ backgroundColor: '#CCCC' }}>
      <Col xs={6} sm={3} md={2}>Epargne handicap</Col>
      <Col xs={1}>
        <Control.checkbox model=".informations.particularites.estAdhesionHandicap" />
      </Col>
      <Col xs={6} sm={3} md={2}>Mineur placé sous administration légal</Col>
      <Col xs={1}>
        <Control.checkbox model=".informations.particularites.estMineurSousAdministrationLegale" /></Col>
      <Col xs={6} sm={3} md={2}>Sous régime de protection</Col>
      <Col xs={1}>
        <Control.checkbox model=".informations.particularites.estSousRegimeDeProtection" /></Col>
    </Row>
  );

  public buildSectionCoordContact = <K extends keyof Dossier, L extends keyof Dossier[K]>(a: K, b: L): any => (
    <Row style={{ backgroundColor: '#CCCC' }}>
      <Col xs={6} sm={3} md={2}>Email</Col>
      <Col xs={6} sm={3} md={2}>{
        <Control.text
          mapProps={{
            style: ({ fieldValue }) => ({ color: fieldValue.valid ? 'black' : 'red' }),
          }}
          model=".informations.coordonneesContact.eMail"
          validators={this.emailValidator}
        />
      }
      </Col>
      <Col xs={6} sm={3} md={2}>Portable</Col>
      <Col xs={6} sm={3} md={2}>{<Control.text model=".informations.coordonneesContact.numeroTelephone" />}</Col>
    </Row>
  )

  private buildChapitreQCE = () => (
    <Chapitre title="Questionnaire conseil épargne">
      <Row style={{ backgroundColor: '#CCCC' }}>
        <Col xs={6} sm={3} md={2}>Poursuivre la pre-souscription:</Col>
        <Col xs={6} sm={3} md={2}>avec QCE</Col>
        <Col xs={6} sm={3} md={2}>{<Control.radio model=".qce" />}</Col>
        <Col xs={6} sm={3} md={2}>sans QCE</Col>
        <Col xs={6} sm={3} md={2}><Control.radio model=".qce" /></Col>
      </Row>
    </Chapitre>
  );

  private buildAdhesion = () => (
    <Chapitre title="Adhesion" >
      <Row style={{ backgroundColor: '#CCCC' }}>
        <Row>
          <Col xs={2} >{<Control.radio model=".wazza" />}</Col>
          <Col xs={10}>le societaire accepte d'adhérer electroniquement:</Col>
        </Row>
        <Row>
          <Col xs={2} xsOffset={2}>Process initial</Col>
          <Col xs={10} xsOffset={10}><Control.radio model=".tazza" /></Col>
        </Row>
        <Row>
          <Col xs={2}><Control.radio model=".wazza" /></Col>
          <Col xs={10}>Le societaire n'accepte pasd'a</Col>
        </Row>
      </Row>
    </Chapitre>
  );

  private buildCodePointdeVenteSeeker = () => {
    const { dossier: { conseiller: { codePointDeVente } }, dispatchChangeAction } = this.props
    const items = [{ id: 'ABCD', label: 'ABCD' }, { id: 'BCDA', label: 'BCDA' }, { id: 'SERRT', label: 'SERRT' }]
    // tslint:disable-next-line:jsx-wrap-multiline
    return <Autosuggest
      datalist={items}
      datalistOnly={false}
      placeholder="Choisissez..."
      value={codePointDeVente}
      valueIsItem={true}
      //itemAdapter={CountryAdapter.instance}
      itemValuePropName="label"
      onChange={
        (e: any) => {
          console.log(e);
          return dispatchChangeAction('dossier.conseiller.codePointDeVente', e)
        }
      }
    />
  }
}

const mapStateToProps = (state: AppState) => ({
  dossier: state.dossier,
  step: state.root.step,
});

const mapDispatchToProps = (dispatch: Dispatch<Props>) => ({
  dispatchChangeAction: (model: string, value: string) => dispatch(actions.change(model, value)),
  dispatchOuvrirDossierAction: (dossierId: string) => dispatch(ouvrirDossierExistantAction(dossierId)),
})

export default connect(mapStateToProps, mapDispatchToProps)(VerificationForm);
