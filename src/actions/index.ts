import { Action } from 'redux';
import Dossier from 'cat-amania-poc-vie-dossier/dist';

export interface CommonAction<P> extends Action {
    type: ActionTypes
    payload?: P
}

export interface TypeActions {
    NONE: string
    LOGIN: string
    LOGOUT: string
    LOGIN_SUCCESS: string,
    LOGIN_FAILURE: string,
    CREATE_NEW_DOSSIER: string,
    OUVRIR_DOSSIER: string,
    OUVRIR_DOSSIER_SUCCESS: string
    OPEN_DOSSIER_FAILURE: string,
    SAVED_MODIFIED_FIELD: string,
}

export type ActionTypes = keyof TypeActions

export const buildNoPayloadAction = (type: keyof TypeActions): CommonAction<any> => {
    return { type }
}

export const loginAction = buildNoPayloadAction('LOGIN')

export interface InitData {
    idDossiers: string[],
    idPointsDeVentes: string[]
}

export const loginSuccessAction = (payload: InitData): CommonAction<InitData> => {
    return { type: 'LOGIN_SUCCESS', payload }
}

export const ouvrirDossierExistantAction = (dossierId: string): CommonAction<string> => {
    return { type: 'OUVRIR_DOSSIER', payload: dossierId }
}

export const openDossierSuccessAction = (payload: Dossier): CommonAction<Dossier> => {
    return { type: 'OUVRIR_DOSSIER_SUCCESS', payload }
}

export const validerVerification = (payload: number): Action => {
    return {
        type: 'validerVerification'
    }
}