import * as React from 'react'
import { Row, Panel } from 'react-bootstrap'

interface Props {
    title: string;
}

class Chapitre extends React.Component<Props> {

    render() {
        const { children, title } = this.props;
        return (
            <Row>
                <Panel bsStyle="primary" id="collapsible-panel-example-1" defaultExpanded={true} >
                    <Panel.Heading>
                        <Panel.Title componentClass="h3" toggle={true} >{title}</Panel.Title>
                    </Panel.Heading>
                    <Panel.Collapse>
                        <Panel.Body style={{ backgroundColor: '#CCCC' }}>{children}</Panel.Body>
                    </Panel.Collapse>
                </Panel>
            </Row>
        );
    }
}

export default Chapitre;
