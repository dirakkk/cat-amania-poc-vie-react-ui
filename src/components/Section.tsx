import * as React from 'react';

interface Props {
    couleur: string;
    title?: string;
}

const sectionStyle = {
    height: 100,
    margin: 20,
    color: 'black',
    borderColor: '#333',
    borderWith: 3,
    borderStyle: 'solid',
};

const titleStyle = {
    marginTop: -10,
    marginLeft: 10,
    backgroundColor: 'white',
    display: 'table'
};

class Section extends React.Component<Props> {

    render() {
        const { couleur, title, children } = this.props;
        return (
            <div style={{ ...sectionStyle, backgroundColor: couleur }}>
                {title ? <div style={titleStyle}>{title}</div> : null}
                {children}
            </div>
        );
    }
}

export default Section;