import * as React from 'react'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import Dossier from 'cat-amania-poc-vie-dossier'

import { Button, Grid, Row, Col } from 'react-bootstrap'
import { actions } from 'react-redux-form'

interface Props {
  dispatchLoginRequestAction: () => any,
  dossier: Dossier,
}

export class Footer extends React.Component<Props> {

  public validerFormulaire = () => {
    console.log('valider')
    actions.submit('dossier')
    actions.change('user.phones[1].type', 'mobile')
  }

  public boutonPoursuivre = () => 
    <Button bsStyle="primary" type="submit" onClick={this.validerFormulaire}>Poursuivre</Button>

  public boutonCorriger = () => <Button bsStyle="primary">CorrigerDansRelation</Button>

  public render() {
    return (
      <Grid>
        <Row>
          <Col xs={3} >{this.boutonCorriger()} </Col>
          <Col xs={3} xsOffset={6} >{this.boutonPoursuivre()} </Col>
        </Row>
      </Grid>
    )
  }

}

// REDUX
// -------------------------------

const mapStateToProps = (state: Dossier) => ({
  dossier: state,
})

const mapDispatchToProps = (dispatch: Dispatch<Props>) => ({
  dispatchLoginRequestAction: () => dispatch(actions.focus('dossier')),
})

export default connect(mapStateToProps, mapDispatchToProps)(Footer)
