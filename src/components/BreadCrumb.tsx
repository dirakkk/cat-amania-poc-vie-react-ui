import * as React from 'react'
import { Row } from 'react-bootstrap'

interface Props {
    step: number;
}

// based on https://bootsnipp.com/snippets/y4Qa

class BreadCrumb extends React.Component<Props> {
    render() {
        const steps = ['Verification', 'Personne', 'Q.Conseil epargne', 'Contrat', 'Validation']
        return (
          <Row style={{marginTop: 10, marginBottom: 10}}>
              <div className="btn-group btn-breadcrumb">
                  <a href="#" className="btn btn-default"><i className="glyphicon glyphicon-home"/></a>
                  { steps.map( (s: string, it: number) =>
                  <a href="#" key={it}className={this.getStyle(it)}>{s}</a> )}
              </div>
        </Row>
        )
    }
    private getStyle = (componentStep: number): string => {
        const { step } = this.props
        return componentStep === step ? 'btn btn-primary' : 'btn btn-default'
    }
}

export default BreadCrumb;
