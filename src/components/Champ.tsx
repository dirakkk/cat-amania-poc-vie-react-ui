import * as React from 'react'
import { Col } from 'react-bootstrap'

interface Props {
    libelle: string;
}

class Champ extends React.Component<Props> {
    render() {
        const { libelle, children } = this.props;
        return (
            <Col><Col>{`${libelle}:`}</Col><Col>{children}</Col></Col>
        )
    }
}

export default Champ;