import * as React from 'react'
import './App.css'
import Section from './components/Section'
import Chapitre from './components/Chapitre'
import Champ from './components/Champ'
import { Route, Switch } from 'react-router-dom'
import { Control } from 'react-redux-form'
import VerificationForm from './forms/VerificationForm';
import LoginForm from './forms/LoginForm';
import ChoixSessionForm from './forms/ChoixSessionForm';

class Personne extends React.Component {
  render() {
    return (
      <div className="App">
        <Chapitre title="Identification">
          <Section couleur="#CCCC" title="Adhérent" >
            <Champ libelle="Numéro de sociétaire" >{<Control.text model=".numSocietaire" />} </Champ>
            <Champ libelle="Lien sociétaire" >{<Control.text model=".lienSocietaire" />} </Champ>
            <Champ libelle="Numéro adhérent parnasse Maif" >{<Control.text model=".numeroAdherent" />} </Champ>
          </Section>
        </Chapitre>
      </div>
    )
  }
}

class App extends React.Component {
  render() {
    return (
      <main>
        <Switch>
          <Route exact={true} path="/" component={LoginForm} />
          <Route path="/choixsession" component={ChoixSessionForm} />
          <Route path="/verification/:dossier" component={VerificationForm} />
          <Route path="/personne" component={Personne} />
        </Switch>
      </main>
    )
  }
}

export default App
