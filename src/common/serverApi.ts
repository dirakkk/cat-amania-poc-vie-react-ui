import { InitData } from '../actions/index'
import Dossier from 'cat-amania-poc-vie-dossier/dist';
import { omit } from 'lodash'

interface EmailPassword {
    email: string,
    password: string
}

const baseUrl = 'http://localhost:3000/api/latest'

export function login({ email, password }: EmailPassword): Promise<InitData> {
    const urlPdv = `${baseUrl}/pointdeventes`
    const pdvRequest = fetch(urlPdv).then((resp: Response) => resp.json())
    const urlDossiers = `${baseUrl}/dossiers`
    const dossiersRequest = fetch(urlDossiers).then((resp: Response) => resp.json())
    const initData: InitData = { idPointsDeVentes: [], idDossiers: [] }
    return Promise.all([pdvRequest, dossiersRequest]).then(function (values: any) {
        initData.idPointsDeVentes = buildListIds(values[0])
        initData.idDossiers = buildListIds(values[1])
        return initData;
    })
}

function buildListIds(response: any[]): string[] {
    console.log('buildListIds', response)
    return response.map(res => res.id)
}

export function getDossier(idDossier: string): Promise<Dossier> {
    const urlDossiers = `${baseUrl}/dossiers/2`
    const dossiersRequest = fetch(urlDossiers).then((resp: Response) => resp.json())
    return dossiersRequest.then(function (json: any) {
        return omit(json, 'links')
    })
}
